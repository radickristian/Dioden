	var $parallaxModuleElements = $( '.parallax-module' );
	if ( $parallaxModuleElements.length ) {
		var prefixedTransform = window.vendorPrefix( 'transform' );
		var parallaxData = [];
		var maxScroll = $( 'body' ).height() - windowHeight;
		var mouseData = {
			x: windowWidth / 2, // Current x-axis position of mouse
			y: windowHeight / 2, // Current y-axis position of mouse
		};
		var lastID = -1;
		$parallaxModuleElements.each( function() {
			var $element = $( this );
			var id = lastID + 1;
			var offset = $element.offset();
			lastID = id;
			parallaxData[ id ] = {
				$el: $element,
				text: $( '.parallax-module__content', $element )[ 0 ],
				image: $( '.parallax-module__image', $element )[ 0 ],
				top: offset.top,
				left: offset.left,
				width: $element.width(),
				height: $element.height(),
			};
		} );
		$( document ).on( 'mousemove', function( event ) {
			mouseData.x = event.clientX || event.pageX;
			mouseData.y = event.clientY || event.pageY;
		} );
		var roundValueForTransform = function( value ) {
			return ( ( value * 1000 ) << 0 ) / 1000;
		};
		var tickId;
		var isTicking = false;
		var tick = function() {
			var scrollTop = window.getScrollTop(),
					localX = 0,
					localY = 0,
					transformX = 0,
					transformY = 0,
					scrollTransformY = 0,
					l = parallaxData.length,
					i,
					item;
			for ( i = 0; i < l; i += 1 ) {
				item = parallaxData[ i ];
				if (
					// Element's bottom above viewport top.
					( item.top + item.height ) < scrollTop ||
					// Element's top below viewport bottom.
					( item.top - windowHeight ) > scrollTop
				) {
					continue;
				}
				localX = mouseData.x - item.left;
				localY = mouseData.y - ( item.top - scrollTop );
				transformX = item.width * (
					( ( item.width / 2 ) - localX ) /
					( item.width / 2 )
				);
				transformY = item.height * (
					( ( item.height / 2 ) - localY ) /
					( item.height / 2 )
				);
				scrollTransformY = item.height *
					( ( item.top + ( item.height / 2 ) ) - scrollTop ) /
					maxScroll;
				transformX *= ( i + 1 ) % 2 === 0 ? -1 : 1;
				item.text.style[ prefixedTransform ] = 'translate(' +
					roundValueForTransform( -0.01 * transformX ) + 'px, ' +
					roundValueForTransform( 0.01 * transformY + 0.2 * scrollTransformY ) + 'px) ' +
					'translateZ(0)';
				item.image.style[ prefixedTransform ] = 'translate(' +
					roundValueForTransform( 0.02 * transformX ) + 'px, ' +
					roundValueForTransform( 0.02 * transformY + 0.4 * scrollTransformY ) + 'px) ' +
					'translateZ(0)';
			}
			tickId = requestAnimationFrame( tick );
		};
		var runTicking = function() {
			if ( isTicking ) {
				return;
			}
			isTicking = true;
			tickId = requestAnimationFrame( tick );
		};
		var stopTicking = function() {
			if ( ! isTicking ) {
				return;
			}
			isTicking = false;
			cancelAnimationFrame( tickId );
			var l = parallaxData.length,
					i,
					item;
			for ( i = 0; i < l; i += 1 ) {
				item = parallaxData[ i ];
				item.text.style[ prefixedTransform ] = '';
				item.image.style[ prefixedTransform ] = '';
			}
		};
		var handlePallaxModuleResize = function() {
			if ( windowWidth >= 992 ) {
				var i, l = parallaxData.length, $element, offset;
				maxScroll = $( 'body' ).height() - windowHeight;
				for ( i = 0; i < l; i += 1 ) {
					$element = parallaxData[ i ].$el;
					offset = $element.offset();
					parallaxData[ i ].top = parseInt( offset.top, 10 );
					parallaxData[ i ].left = parseInt( offset.left, 10 );
					parallaxData[ i ].width = parseInt( $element.width(), 10 );
					parallaxData[ i ].height = parseInt( $element.height(), 10 );
				}
				runTicking();
			} else {
				stopTicking();
			}
		};
		$( window ).on( 'resize', handlePallaxModuleResize );
		handlePallaxModuleResize();
	}